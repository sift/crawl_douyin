#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Contact :   liuyuqi.gov@msn.cn
@Time    :   2023/10/20 16:30:32
@License :   Copyright © 2017-2022 liuyuqi. All Rights Reserved.
@Desc    :   video model
'''
import os,sys,requests,json,time,random,hashlib,base64,urllib,urllib3

class Video(object):
    ''' video model '''

    def __init__(self):
        pass