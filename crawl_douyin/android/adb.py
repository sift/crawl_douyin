#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Auther :liuyuqi.gov@msn.cn
@Time :2018/6/15 4:46
@File :adb.py
'''
import os
import platform
import subprocess


class ADB():
    '''  '''
    def __init__(self):
        # adb.exe存放项目adb目录中，或者安装 android 开发环境
        try:
            path = "adb"
            subprocess.Popen(path, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            self.path = path
        except Exception as e:
            if platform.system() == "Windows":
                path = os.path.join("adb", "adb.exe")
                try:
                    subprocess.Popen(path, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                except Exception as e:
                    print(e)
            else:
                try:
                    subprocess.Popen(path, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                except Exception as e:
                    print(e)

    def run(self):
        print("test")

    def take_screen(self):
        pass

    def set_path(self, path):
        self.path = path

    def get_path(self):
        return self.path
