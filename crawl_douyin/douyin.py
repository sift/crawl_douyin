#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Contact :   liuyuqi.gov@msn.cn
@Time    :   2023/10/20 16:01:49
@License :   Copyright © 2017-2022 liuyuqi. All Rights Reserved.
@Desc    :   douyin
'''
import os,sys,re,requests
from .cookie import get_new_cookie
import json, math, time, random
js = execjs.compile(open(r'./static/dy.js', 'r', encoding='gb18030').read())

class Douyin(object):
    ''' 抖音 '''

    _headers = {
        "authority": "www.douyin.com",
        "accept": "application/json, text/plain, */*",
        "accept-language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
        "cache-control": "no-cache",
        "pragma": "no-cache",
        "referer": "https://www.douyin.com/user/MS4wLjABAAAAigSKToDtKeC5cuZ3YsDrHfYuvpLqVSygIZ0m0yXfUAI",
        "sec-ch-ua": "\"Microsoft Edge\";v=\"117\", \"Not;A=Brand\";v=\"8\", \"Chromium\";v=\"117\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36 Edg/117.0.2045.47"
    }

    def __init__(self):
        self.sess= requests.Session()
        self.sess.headers = self._headers
        self.get_cookie()

        self.user_list = []
        self.video_list = []
        self.info = {}

    def download_video(self):
        pass
    
    def search(self, keyword:str, sort_type:int , publish_time:int, number = 20):
        ''' 搜索并下载视频
            :param keyword: 关键词
            :param sort_type: 排序方式,  0 智能排序, type='1' 热门排序, type='2' 最新排序
            :param number: 下载数量, 
            :param publish_time: 发布时间 ,0为不限时间，1是1天内的视频，666是666天内的视频
        '''
        params = {
            "device_platform": "webapp",
            "aid": "6383",
            "channel": "channel_pc_web",
            "search_channel": "aweme_general",
            "sort_type": "0",
            "publish_time": "0",
            "keyword": "阔澜 可以点评四周的风景",
            "search_source": "normal_search",
            "query_correct_type": "1",
            "is_filter_search": "0",
            "from_group_id": "",
            # "offset": "15",
            # "count": "25",
            # "search_id": "202310162344486FA7398D503D9C3E9846",
            "pc_client_type": "1",
            "version_code": "190600",
            "version_name": "19.6.0",
            "cookie_enabled": "true",
            "screen_width": "1707",
            "screen_height": "1067",
            "browser_language": "zh-CN",
            "browser_platform": "Win32",
            "browser_name": "Edge",
            "browser_version": "118.0.2088.46",
            "browser_online": "true",
            "engine_name": "Blink",
            "engine_version": "118.0.0.0",
            "os_name": "Windows",
            "os_version": "10",
            "cpu_core_num": "20",
            "device_memory": "8",
            "platform": "PC",
            "downlink": "10",
            "effective_type": "4g",
            "round_trip_time": "50",
            "webid": "",
            "msToken": "",
        }
        params['keyword'] = keyword
        params['sort_type'] = sort_type
        params['publish_time'] = publish_time
        params['count'] = number
        params['webid'] = self.info['webid']

        while len(self.video_list) <number:
            res= self.sess.get("https://www.douyin.com/aweme/v1/web/general/search/single/",
                               params=params,)
            
        splice_url_str  = self.splice_url(params)
        xs = js.call('get_dy_xb', splice_url_str)

    @staticmethod
    def splice_url(params):
        splice_url_str = ''
        for key, value in params.items():
            splice_url_str += key + '=' + value + '&'
        return splice_url_str[:-1]

    def get_cookie(self):
        ''' 获取cookie '''
        # read cookie.json, if empty then get cookie
        try:
            with open("cookie.json","r", encoding="utf8") as file:
                cookie = file.read()
            self.sess.cookies=cookie
        except Exception as e:
            print("正在打开chrome浏览器,请等待2-5s，如长时间无反应，请反馈给我们")
            cookie=get_new_cookie()
            self.get_cookie()
