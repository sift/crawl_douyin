#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Auther :liuyuqi.gov@msn.cn
@Time :2018/6/21 3:53
@File :ai.py
'''

import os
import pandas as pd
from sklearn.linear_model import LinearRegression


def init():
    global distances, press_times
    distances = []
    press_times = []
    if os.path.exists("./jump_range.csv"):
        distances, press_times = get_data("./jump_range.csv")
    else:
        save_data("./jump_range.csv", [], [])
        return 0


def get_data(file_name):
    data=pd.read_csv(file_name)
    distances_array=[]
    press_times_array=[]
    for distance,press_time in zip(data["Distance"],data["Press_time"]):
        distances_array.append([float(distance.strip("[]"))])
        press_times_array.append([float(press_time.strip("[]"))])
    return distances_array,press_times_array

def save_data():
    pass


def add_data():
    distances.append()


def get_result_len():
    return len(distances)


def linear_model_main(_distances, _press_times, target_distance):
    regr = LinearRegression()
    regr.fit(_distances, _press_times)
    predict_press_times = regr.predict(target_distance)
    # 定义结果字典
    result = {}
    result["intercept"] = regr.intercept_


def computing_k_b_v(target_distance):
    result = linear_model_main(target_distance=target_distance)
