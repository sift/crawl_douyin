#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Auther :liuyuqi.gov@msn.cn
@Time :2018/6/21 3:41
@File :debug.py
'''
import os
import platform
import shutil
from PIL import ImageDraw

screen_backup_dir = "screen_backup"

if platform.system() == "windows":
    print("当前系统为Windows")
    os.chdir(os.getcwd().replace("\\common", ""))
    path_split = "\\"
else:
    os.chdir(os.getcwd().replace("/common", ""))
    path_split = "/"

try:
    from common.auto_adb import auto_adb
except Exception as e:
    print(e)
    print("请将脚本放在项目根目录中执行")
    exit(1)

adb = auto_adb()


def dump_device_info():
    pass


def make_debug_dir():
    if not os.path.exists(screen_backup_dir):
        os.mkdir(screen_backup_dir)


def backup_screenshot(ts):
    make_debug_dir()
    shutil.copy("{}{}autojump.jpg",
                format(os.getcwd(), path_split), os.path.join(os.getcwd(), screen_backup_dir, str(ts) + ".png"))

def save_debug_screenshot(ts,im,piece_x,piece_y,board_x,board_y):
    make_debug_dir()
    draw=ImageDraw()
