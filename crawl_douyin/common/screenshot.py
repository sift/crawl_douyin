#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Auther :liuyuqi.gov@msn.cn
@Time :2018/6/21 20:21
@File :screenshot.py
'''

import subprocess
import os,sys
from PIL import ImageDraw,Image
from io import StringIO
try:
    from common.auto_adb import auto_adb
except Exception as e:
    print(e)
    exit(-1)

def pull_screenshot():
    global SCREENSHOT_WAY
    if SCREENSHOT_WAY >= 1 & SCREENSHOT_WAY <= 3:
        process = subprocess.Popen(adb.adb_path + " shell screencap -p")
        if SCREENSHOT_WAY == 2:
            binary_screenshot = binary_screenshot.replace(b"\r\r\n", b"\n")
        else:
            binary_screenshot = binary_screenshot.replace(b"\r\r\n", b"\n")
    elif SCREENSHOT_WAY ==0:
        adb.run()
        return Image.open("./autojump.jpg")

def check_screenshot():
    '''

    :return:
    '''
    global SCREENSHOT_WAY
    if os.path.isfile("autojump.jpg"):
        try:
            os.remove("autojump.jpg")
        except Exception as e:
            print(e)
