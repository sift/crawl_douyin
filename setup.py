from setuptools import setup, find_packages

with open('requirements.txt') as f:
    required = f.read().splitlines()

with open('README.md') as f:
    readme = f.read()

setup(
    name='pytorch-rl',
    version='0.1.0',
)

