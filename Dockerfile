FROM python:alpine
LABEL Name=douyinhelper Version=1.0.1

WORKDIR /app
ADD . /app

RUN  apk add nodejs
RUN python3 -m pip install -r requirements.txt
CMD ["python3", "-m", "douyinhelper"]
