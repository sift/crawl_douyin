#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Contact :   liuyuqi.gov@msn.cn
@Time    :   2023/10/20 16:00:56
@License :   Copyright © 2017-2022 liuyuqi. All Rights Reserved.
@Desc    :   enter point
'''
import os,sys,re,time,argparse
from crawl_douyin import Douyin

def parse_args():
    parser = argparse.ArgumentParser(description='douyin')
    parser.add_argument('cmd', metavar='COMMAND', nargs='?', help='subcommand to run (default: %(default)s)', default='search')
    parser.add_argument('--version', action='version', version='%(prog)s')
    parser.add_argument('--debug', action='store_true', help='debug mode')

    return parser.parse_args()

if __name__=='__main__':
    args=parse_args()

    douyin = Douyin()
    if args.command =="video":
        pass
    elif args.command =="live":
        if args.cmd == "gift":
            pass
        elif args.cmd == "comment":
            pass
        elif args.cmd == "danmu":
            pass
