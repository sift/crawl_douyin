#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Auther :liuyuqi.gov@msn.cn
@Time :2018/6/22 23:28
@File :douyin_comment.py
'''

import sys

try:
    from common import debug, screenshot
    from
except Exception as e:
    print(e)
    exit(-1)

VERSION = "1.0.0.0"
APPID = ""
APPKEY = ""
DEBUG = True
FACE_PATH = "face/"
WORK_SPACE = ""
DATA_PATH = ""

def prepare():
    '''
    准备工作
    :return:
    '''
    while True:
        title = str(input("请ADB链接手机和电脑，确定开始？[y/n]："))
        if title == "y":
            print("DouYin项目版本:", format(VERSION))
            print("按CONTROL+C组合键退出")
            break
        elif title == "n":
            print("已退出程序")
            exit(1)
        else:
            print("请重新输入：")

def comment():
    pass

def main():
    '''
    主程序
    :return:
    '''

    # adb 截屏，识别图片中三点，点击三点，输入指定文字，下滑手机重复操作。
    adb=adb()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)
        exit(0)
