## 抖音接口

目前抖音已经有了开放平台，可以付费调用。这里说的接口文档是模拟用户登录后的API接口。既然是模拟登录，那么就需要破解签名算法。


```
const apiBase = "http://api.amemv.com/aweme/v1/"
const apiBaseAuth = "https://lf.snssdk.com/"

const API_NAME_FOLLOWERS = "user/follower/list"
const API_NAME_FOLLOWEINGS = "user/following/list"
const API_NAME_USER = "user"
const API_NAME_POSTS = "aweme/post"
const API_NAME_FAVORITES = "aweme/favorite"
const API_NAME_FEEDS = "feed"
const API_NAME_NEARBY_FEEDS = "nearby/feed"
const API_NAME_CATEGORY_LIST = "category/list"
const API_NAME_MUSIC = "music/aweme"
const API_NAME_MUSIC_NEW = "music/fresh/aweme"
const API_NAME_SEARCH_ALL = "general/search"
const API_NAME_SEARCH_USER = "discover/search"
const API_NAME_SEARCH_MUSIC = "music/search"
const API_NAME_SEARCH_CHALLENGE = "challenge/search"

const API_NAME_AUTH_LOGIN_MOBILE = "user/mobile/login/v2"


http://v.douyin.com/M125ff/


https://www.iesdouyin.com

https://aweme.snssdk.com/aweme/v1/aweme/detail/

https://sso.douyin.com

```





## 

