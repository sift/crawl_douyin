#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Auther :liuyuqi.gov@msn.cn
@Time :2018/6/15 14:59
@File :write_cfg.py
'''
import os
from configparser import ConfigParser

config_file = os.path.join("conf.ini")
config = ConfigParser()

config["APP_INFO"] = {
    "VIRSION": "1.0.0.0"
}
config["QQ_AI"] = {
    "APPID": "123",
    "APPKEY": "456"
}


def saveConf():
    with open(config_file, "w") as f:
        config.write(f)


def readConf():
    with open(config_file, "r") as f:
        config.read(f)
        print("版本号：", config.get("APP_INFO", "VIRSION"))


readConf()
