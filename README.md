# douyin_crawl

- [x] 视频(去水印)、图片、主页、搜索、用户信息爬取
- [x] 抖音辅助，自动评论，点赞已经关注的朋友的视频。
- [ ] UI界面，可视化操作，可视化爬取数据


## Usage

源码执行：
```
virtualenv .venv
.venv/bin/activate
pip install -r requirements.txt

# 下载用户列表所有视频
python main.py video --cmd download --user_list_file user_list.txt 
python main.py video --cmd search --keyword 美女

# 弹幕和礼物
python main.py live --cmd gift --room_id 666666
python main.py live --cmd danmu --room_id 666666
```

安装包方式：
```
pip install crawl_douyin
crawl_douyin video --cmd download --user_list_file user_list.txt
```

docker方式：
```
docker build -t crawl_douyin .
docker run -it crawl_douyin
```

## Develop

```
pip install crawl_douyin

douyin = Douyin()
```

## Reference

- [DouYin_Spider](https://github.dev/cv-cat/DouYin_Spider)



## License

Licensed under the [Apache 2.0](LICENSE) © [liuyuqi.gov@msn.cn](https://github.com/jianboy)





